jQuery(document).ready(function($){
	
	// Tabs
	if($('.content-tabs')) {
		
		// switch tabs/content on click
		$('.content-tabs .tab-nav .tab').on('click', function(){
			
			// remove all active
			$('.content-tabs .tab-nav .tab').removeClass('active');

			// add active to this
			$(this).addClass('active');

			// hide all content
			$('.content-tabs .tab-content-wrap').hide();
			var id = $('span', this).attr('data-id');
			
			// fade in new content
			$(id).fadeIn('fast');
		});
		
		// toggle open
		$(document).on('click','.tab-nav.stacked li', function(){
			$(this).closest('.stacked').toggleClass('open');
			$(this).prependTo($(this).closest('.stacked'));
		});
		
		// check for stack or not
		$(window).on('resize', function() {
			$('.tab-nav').each(function(){
				if($(this).height() > $('.tab', this).height()){
					$(this).addClass('stacked');
				}
			});
		}).resize();	
	}
	
	// Accordions
	if($('.accordion')){
		$('.accordion-title.has-content').on('click',function(){

			// check if open
			if($(this).hasClass('open')){
				$(this).removeClass('open');
				$(this).next('.accordion-content').slideUp('fast');
			}else{
				$(this).addClass('open');
				$(this).next('.accordion-content').slideDown('fast');
			}
		});
	}
});