<?php 
	
	add_action( 'init', 'cptui_register_my_cpts_resource' );
	function cptui_register_my_cpts_resource() {
		$labels = array(
			"name" => __( 'Resources', 'modern-org-child' ),
			"singular_name" => __( 'Resource', 'modern-org-child' ),
		);

		$args = array(
			"label" => __( 'Resources', 'modern-org-child' ),
			"labels" => $labels,
			"description" => "",
			"public" => true,
			"publicly_queryable" => true,
			"show_ui" => true,
			"show_in_rest" => false,
			"rest_base" => "",
			"has_archive" => true,
			"show_in_menu" => true,
			"exclude_from_search" => false,
			"capability_type" => "post",
			"map_meta_cap" => true,
			"hierarchical" => false,
			"rewrite" => array( "slug" => "resource", "with_front" => true ),
			"query_var" => true,
			"supports" => array( "title", "editor", "thumbnail" )				
		);
		register_post_type( "resource", $args );
	}

	add_action( 'init', 'cptui_register_my_taxes_resource_category' );
	function cptui_register_my_taxes_resource_category() {

		// custom resource cats
		$labels = array(
			"name" => __( 'Categories', 'modern-org-child' ),
			"singular_name" => __( 'Category', 'modern-org-child' ),
		);

		$args = array(
			"label" => __( 'Categories', 'modern-org-child' ),
			"labels" => $labels,
			"public" => true,
			"hierarchical" => true,
			"label" => "Categories",
			"show_ui" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"query_var" => true,
			"rewrite" => array( 'slug' => 'resource-category', 'with_front' => true, ),
			"show_admin_column" => false,
			"show_in_rest" => false,
			"rest_base" => "",
			"show_in_quick_edit" => false,
		);
		register_taxonomy( "resource_category", array( "resource" ), $args );
	}

	// custom fields for resources
	if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array (
		'key' => 'group_58582cf54f25f',
		'title' => 'Resource fields',
		'fields' => array (
			array (
				'layout' => 'vertical',
				'choices' => array (
					'page' => 'Link to page',
					'direct' => 'Direct Download',
				),
				'default_value' => '',
				'other_choice' => 0,
				'save_other_choice' => 0,
				'allow_null' => 0,
				'return_format' => 'value',
				'key' => 'field_58583ea229ff1',
				'label' => 'Resource Link Type',
				'name' => 'resource_link_type',
				'type' => 'radio',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '25',
					'class' => '',
					'id' => '',
				),
			),
			array (
				'layout' => 'vertical',
				'choices' => array (
					'pdf' => 'PDF File',
					'link' => 'Link URL',
					'image' => 'Image File',
					'video' => 'Video URL',
					'audio' => 'Audio File',
					'doc' => 'Document',
					'ppt' => 'Presentation',
					'excel' => 'Spreadsheet',
					'other' => 'Other File',
				),
				'default_value' => '',
				'other_choice' => 0,
				'save_other_choice' => 0,
				'allow_null' => 0,
				'return_format' => 'value',
				'key' => 'field_58582cfd9fbd2',
				'label' => 'Resource Type',
				'name' => 'resource_type',
				'type' => 'radio',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '25',
					'class' => '',
					'id' => '',
				),
			),
			array (
				'width' => '',
				'height' => '',
				'key' => 'field_58582f0b9fbd3',
				'label' => 'Video URL',
				'name' => 'video_url',
				'type' => 'oembed',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'field_58582cfd9fbd2',
							'operator' => '==',
							'value' => 'video',
						),
					),
				),
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
			),
			array (
				'default_value' => '',
				'placeholder' => 'http://',
				'key' => 'field_58582f519fbd4',
				'label' => 'Link URL',
				'name' => 'link_url',
				'type' => 'url',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'field_58582cfd9fbd2',
							'operator' => '==',
							'value' => 'link',
						),
					),
				),
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
			),
			array (
				'return_format' => 'array',
				'preview_size' => 'thumbnail',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
				'key' => 'field_5858301d9fbd6',
				'label' => 'Image File',
				'name' => 'image_file',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'field_58582cfd9fbd2',
							'operator' => '==',
							'value' => 'image',
						),
					),
				),
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
			),
			array (
				'return_format' => 'array',
				'library' => 'all',
				'min_size' => '',
				'max_size' => '',
				'mime_types' => '',
				'key' => 'field_5858303d9fbd7',
				'label' => 'File Upload',
				'name' => 'file_upload',
				'type' => 'file',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'field_58582cfd9fbd2',
							'operator' => '==',
							'value' => 'pdf',
						),
					),
					array (
						array (
							'field' => 'field_58582cfd9fbd2',
							'operator' => '==',
							'value' => 'audio',
						),
					),
					array (
						array (
							'field' => 'field_58582cfd9fbd2',
							'operator' => '==',
							'value' => 'doc',
						),
					),
					array (
						array (
							'field' => 'field_58582cfd9fbd2',
							'operator' => '==',
							'value' => 'ppt',
						),
					),
					array (
						array (
							'field' => 'field_58582cfd9fbd2',
							'operator' => '==',
							'value' => 'excel',
						),
					),
					array (
						array (
							'field' => 'field_58582cfd9fbd2',
							'operator' => '==',
							'value' => 'other',
						),
					),
				),
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'resource',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));

	endif;
?>