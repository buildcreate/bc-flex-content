<?php if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_58a61a42ef0f9',
	'title' => 'Content Row Options',
	'fields' => array (
		array (
			'select_type' => 2,
			'key' => 'field_58a61a591de7a',
			'label' => 'Post Types using "Content Rows"',
			'name' => 'post_types_using_content_rows',
			'type' => 'post_type_selector',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'site-options',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;