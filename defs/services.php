<?php 
	
add_action( 'init', 'cptui_register_my_cpts_service' );

function cptui_register_my_cpts_service() {
	$labels = array(
		"name" => __( 'Services', 'modern-org-child' ),
		"singular_name" => __( 'Service', 'modern-org-child' ),
		);

	$args = array(
		"label" => __( 'Services', 'modern-org-child' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "service", "with_front" => true ),
		"query_var" => true,
		
		"supports" => array( "title", "editor", "thumbnail" ),					);
	register_post_type( "service", $args );

}

add_action( 'init', 'cptui_register_my_taxes_service_category' );

function cptui_register_my_taxes_service_category() {
	$labels = array(
		"name" => __( 'Categories', 'modern-org-child' ),
		"singular_name" => __( 'Category', 'modern-org-child' ),
		);

	$args = array(
		"label" => __( 'Categories', 'modern-org-child' ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Categories",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'service-category', 'with_front' => true, ),
		"show_admin_column" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "service_category", array( "service" ), $args );

}