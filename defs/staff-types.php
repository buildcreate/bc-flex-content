<?php
	
add_action( 'init', 'cptui_register_my_taxes_staff_type' );
function cptui_register_my_taxes_staff_type() {
	$labels = array(
		"name" => __( 'Staff Types', '' ),
		"singular_name" => __( 'Staff Type', '' ),
		);

	$args = array(
		"label" => __( 'Staff Types', '' ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Staff Types",
		"show_ui" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'staff_type', 'with_front' => true ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "staff_type", array( "staff" ), $args );

// End cptui_register_my_taxes_staff_type()
}
	
?>