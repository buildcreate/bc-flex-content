<section class="content-carousel">
	<div class="page-content">
		
		<div class="carousel">
			<?php while(have_rows('slides')) : the_row(); ?>
			
				<?php 
					$link_type = get_sub_field('link_type');
					$url = get_sub_field('url');
					$content = get_sub_field('content');
				?>
			
				<div class="carousel-item<?php if($content){echo ' has-content';} ?>" style="display:none;">
					<div class="carousel-item-wrap">
						<?php if($link_type == 'slide' && $url) : ?>
							<a href="<?php echo $url; ?>">
						<?php endif; ?>
						
						<img src="<?php echo get_sub_field('image'); ?>" />
						
						<?php if($content) : ?>
							<div class="carousel-content">
								<span>
									<?php echo $content; ?>
									<?php if($link_type == 'button' && $url) : ?>
										<a href="<?php echo $url; ?>">Learn More</a>
									<?php endif; ?>
								</span>
							</div>
						<?php endif; ?>
						
						<?php if($link_type == 'slide' && $url) : ?>
							</a>
						<?php endif; ?>
					</div>
				</div>
					
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		</div>
	</div>
	
	<?php 
		$num_slides = get_sub_field('number_of_slides_to_show') ? get_sub_field('number_of_slides_to_show') : 4;
		$num_slides_scroll = get_sub_field('number_of_slides_to_scroll') ? get_sub_field('number_of_slides_to_scroll') : 2; 
		$show_arrows = get_sub_field('show_arrows') ? 'true' : 'false'; 
	?>
	
	<script type="text/javascript">
		jQuery(document).ready(function($){
			$('.carousel').slick({
				autoplaySpeed: 4000,
				autoplay: true,
				slidesToShow: <?php echo $num_slides; ?>,
				slidesToScroll: <?php echo $num_slides_scroll; ?>,
				draggable: true,
				arrows: <?php echo $show_arrows; ?>,
				// responsive: [
				// 	    {
				// 	      breakpoint: 800,
				// 	      settings: {
				// 	        slidesToShow: 3,
				// 	        slidesToScroll: 3,
				// 	        infinite: true,
				// 	        dots: false
				// 	      }
				// 	    },
				// 	    {
				// 	      breakpoint: 600,
				// 	      settings: {
				// 	        slidesToShow: 2,
				// 	        slidesToScroll: 2,
				// 	        infinite: true,
				// 	        dots: false
				// 	      }
				// 	    },
				// 		 {
				// 	      breakpoint: 480,
				// 	      settings: {
				// 	        slidesToShow: 1,
				// 	        slidesToScroll: 1,
				// 	        infinite: true,
				// 	        dots: false
				// 	      }
				// 	    }
				// ]
			});
			$('.carousel-item').removeAttr('style');
		});
	</script>

		
	</div>
</section>