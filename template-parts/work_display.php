<section class="content-work-display">
	<h3 class="section-title"><?php echo get_sub_field('title'); ?></h3>
	<?php $grid_cols = get_sub_field('number_of_grid_columns'); ?>
	<?php $work_categories = get_sub_field('work_categories'); 
			$bcflex_options = get_option('flex_settings');
			
			if($service_categories){ 
					$tax_query = array(
						array(
							'taxonomy' => 'work_category',
							'field' => 'ids',
							'terms' => $work_categories
						)
					);			
					
				} else {
					$tax_query = array();
				}
		
	?>
	<?php 
		$args = array(
			'post_type' => 'work',
			'posts_per_page' => -1,
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'tax_query' => $tax_query
		);
		$work = new WP_Query($args);
	?>
	<?php if($services->have_posts()) : ?>
		<?php $count = 0; ?>
		<ul class="<?php if(get_sub_field('grid_type') == 'grid') {echo 'grid';} else {echo 'list';} ?>">
		<?php while($work->have_posts()) : $work->the_post(); ?>
			<li class="work <?php if(get_sub_field('grid_type') == 'grid') {echo grid_class($count, $grid_cols, 1); $count++; } ?>">
				
				<?php if(get_sub_field('grid_type') == 'grid') : ?>
				
					<?php if(has_post_thumbnail()) : ?>
						
						<?php the_post_thumbnail('medium'); ?>
						
						<a class="work-wrap" href="<?php the_permalink(); ?>">
							<h4 class="title"><?php the_title(); ?><br><small><?php the_field('client_name'); ?></small></h4>
													
						</a>	
						
					<?php else : ?>
					
					<?php endif; ?>
					
					
						
					
				<?php else : // listing ?>
					
					<?php if(has_post_thumbnail()) : ?>
						<?php the_post_thumbnail('small'); ?>
					<?php else : ?>
					
					<?php endif; ?>
					<div class="work-wrap">				
						<h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?><br><small><?php the_field('client_name'); ?></small></a></h3>
					</div>
				<?php endif; ?>
			</li>
		<?php endwhile; ?>
			<li class="clearer"></li>
		</ul>
	<?php endif; ?>
</section>
