<section class="content-wysiwyg <?php echo get_sub_field('class'); ?>">
	<div class="page-content">
		<?php echo get_sub_field('wysiwyg'); ?>
	</div>
</section>