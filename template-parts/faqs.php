<?php
	$question_background_color_css = '';
	if($question_background_color = get_sub_field('question_background_color')){
		$question_background_color_css = 'background:'. $question_background_color.';';
	}

	$question_text_color_css = '';
	if($question_text_color = get_sub_field('question_text_color')){
		$question_text_color_css = 'color:'.$question_text_color.';';
	}

	$answer_background_color_css = '';
	if($answer_background_color = get_sub_field('answer_background_color')){
		$answer_background_color_css = 'background:'.$answer_background_color.';';
	}

	$answer_text_color_css = '';
	if($answer_text_color = get_sub_field('answer_text_color')){
		$answer_text_color_css = 'color:'.$answer_text_color.';';
	}
?>
<section class="content-faqs content-block">
	<h3 class="section-title"><?php echo get_sub_field('title'); ?></h3>
	<?php 
		$faq_cats = array();
		if($faq_categories = get_sub_field('faq_categories')){
			foreach($faq_categories as $cat){
				$faq_cats[] = $cat->term_id;
			}
		}

		$args = array(
			'post_type' => 'faqs',
			'posts_per_page' => -1,
			'order' => 'ASC', 
			'orderby' => 'menu_order',
			'tax_query' => array(
				array(
					'taxonomy' => 'faq_categories',
					'field' => 'term_id',
					'terms' => $faq_cats
				)
			)
		);
	?>
	<?php $faqs = new WP_Query($args); ?>
	<?php if($faqs->have_posts()): ?>
		<ul class="accordion faq-list">
			<?php while($faqs->have_posts()) : $faqs->the_post(); ?>
				<li class="accordion-item faq-item">
					<h4 class="accordion-title faq-title has-content" style="<?php echo $question_background_color_css . $question_text_color_css; ?>"><i class="btr bt-angle-down"></i><?php the_title(); ?></h4>
					<div class="accordion-content faq-content user-content" style="display:none;<?php echo $answer_background_color_css . $answer_text_color_css; ?>"><?php the_content(); ?></div>
				</li>
			<?php endwhile; ?>
		</ul>
	<?php endif; ?>
</section>