<section class="content-tabs">
	<h3 class="section-title"><?php echo get_sub_field('title'); ?></h3>
	<?php if(have_rows('tabs')): ?>
		<ul class="tab-nav">
			<?php $first_tab = true; ?>
			<?php while(have_rows('tabs')) : the_row(); ?>
				<li class="tab<?php if($first_tab){echo ' active';} $first_tab = false; ?>">
					<span data-id="#tab-<?php echo str_replace(' ', '_', preg_replace("/[^a-zA-Z0-9\s]/", "", get_sub_field('tab_title'))); ?>"><?php the_sub_field('tab_title'); ?></span>
				</li>
			<?php endwhile; ?>
		</ul>

		<?php $first_content = true; ?>
		<?php while(have_rows('tabs')) : the_row(); ?>
			<div class="tab-content-wrap" id="tab-<?php echo str_replace(' ', '_', preg_replace("/[^a-zA-Z0-9\s]/", "", get_sub_field('tab_title'))); ?>" <?php if(!$first_content){echo ' style="display:none;"';} $first_content = false;?>>
				<?php $type = get_sub_field('type_of_content');  ?>
				<?php if($type == 'custom_content') : ?>
					<div class="user-content tab-content"><?php the_sub_field('content'); ?></div>
				<?php elseif($type == 'content_type') : ?>
					<?php if($tab_content = get_sub_field('select_content')) : ?>
						<?php foreach($tab_content as $a) : ?>
							<h3 class="tab-content-title"><?php echo $a->post_title; ?></h3>
							<div class="user-content tab-content"><?php echo wpautop($a->post_content); ?></div>
						<?php endforeach; ?>
					<?php endif; ?>
				<?php endif; ?>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>	
</section>