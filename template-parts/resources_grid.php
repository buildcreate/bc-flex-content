<section class="content-resource-grid">
	<h3 class="section-title"><?php echo get_sub_field('title'); ?></h3>
	<?php
		// get grid cols
		$grid_cols = get_sub_field('number_of_grid_columns');
		
		// only get selected categories
		$limit_by_category = get_sub_field('limit_by_category');
		if($limit_by_category){ 

			// get categoreis
			$resource_category = get_sub_field('resource_category');
			if(!$resource_category){
				$resource_category = array();
			} 

			$args = array(
				'post_type' => 'resource',
				'posts_per_page' => -1,
				'orderby' => 'menu_order',
				'order' => 'ASC',
				'tax_query' => array(
					array(
						'taxonomy' => 'resource_category',
						'field' => 'ids',
						'terms' => $resource_category
					)
				)
			);
		}else{
			// get all resource posts
			$args = array(
				'post_type' => 'resource',
				'posts_per_page' => -1,
				'orderby' => 'menu_order',
				'order' => 'ASC'
			);
		}
		$resource = new WP_Query($args);
	?>
	
	<?php if($resource->have_posts()) : ?>
		<?php $count = 0; ?>
		<ul class="<?php if(get_sub_field('grid_type') == 'featured') {echo 'grid';} else {echo 'list';} ?>">
		<?php while($resource->have_posts()) : $resource->the_post(); global $post; ?>
			<?php 			
				// get resource and link type
				$link_type = get_post_meta($post->ID, 'resource_link_type', true);
				$resource_type = get_post_meta($post->ID, 'resource_type', true);
				switch($resource_type){
					case 'pdf' :
					case 'audio' :
					case 'document' : 
					case 'ppt' :
					case 'excel' :
					case 'other' :
						$resource_type = 'file';
						$file = get_post_meta($post->ID, 'file_upload', true);
						$the_resource = wp_get_attachment_url($file);
						$icon = '<i class="bts bt-file"></i>';
						break;
					case 'image' : 
						$resource_type = 'image';
						$the_resource = get_post_meta($post->ID, 'image_file', true);
						$icon = '<i class="bts bt-photo"></i>';
						break;
					case 'link' :
						$resource_type = 'link';
						$the_resource = get_post_meta($post->ID, 'link_url', true);
						$icon = '<i class="bts bt-link"></i>';
						break;
					case 'video' :
						$resource_type = 'video';
						$the_resource = get_post_meta($post->ID, 'video_url', true);
						$icon = '<i class="bts bt-play-circle"></i>';
						break;
					default : 
						$resource_type = false;
						break;
				}
			?>
			<?php if($resource_type) : ?>
			<li class="resource-item <?php if(get_sub_field('grid_type') == 'featured') {echo grid_class($count, $grid_cols, 1); $count++; } ?>">
				<div class="resource-wrap">
					<h3 class="title">
						<span class="resource-cat"><?php echo wp_get_post_terms($post->ID, 'resource_category')[0]->name; ?></span>
						
						<?php if($link_type == 'direct') : ?>
							<a target="_blank" href="<?php echo $the_resource; ?>" title="<?php the_title(); ?>"><?php the_title(); ?> <?php echo $icon; ?></a>
						<?php else : ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?> <?php echo $icon; ?></a>
						<?php endif; ?>
					</h3>
					<div class="resource-date"><?php echo date('F jS, Y', strtotime($post->post_date)); ?></div>
				</div>
			</li>
			<?php endif; ?>
		<?php endwhile; ?>
			<li class="clearer"></li>
		</ul>
	<?php endif; ?>
</section>