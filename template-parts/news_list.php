<section class="content-news-list">
	<h3 class="section-title"><?php echo get_sub_field('title'); ?></h3>
	<?php 
		$num_items = get_sub_field('number_of_news_items');
		$cat_ids = get_sub_field('categories');
		if($cat_ids){
			$args = array(
				'post_type' => 'post',
				'posts_per_page' => $num_items,
				'tax_query' => array(
					array(
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $cat_ids
					)
				)
			);
		}else{
			// if no cats selected, show all posts
			$args = array(
				'post_type' => 'post',
				'posts_per_page' => $num_items
			); 
		}
	?>
	<?php $posts = new WP_Query($args); ?>
	<?php if($posts->have_posts()) : ?>
	<ul class="news-list">
		<?php while($posts->have_posts()) : $posts->the_post(); ?>
			<li class="<?php if(has_post_thumbnail()) : echo 'has-image '; endif; ?><?php if(get_sub_field('show_excerpt') == 'show'){echo 'has-excerpt ';} ?>">
				<?php if(has_post_thumbnail()) : ?>
					<?php the_post_thumbnail('thumbnail'); ?>
				<?php endif; ?>
			 	<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
			 	<?php if(get_sub_field('show_excerpt') == 'show'){the_excerpt();} ?>
			 	<span class="clearer"></span>
		 	</li>
		<?php endwhile; ?>
	</ul>
	<?php endif; ?>
</section>