<section class="content-woocommerce-categories">
	<h6><?php the_sub_field('list_title'); ?></h6>
	<?php 
		// get cats/brands
		if(get_sub_field('this_list_is') == 'categories'){
			$cats = get_sub_field('category_list');
			$all_link = '/shop/';
		}else{
			$cats = get_sub_field('brand_list');
			$all_link = '/shop/brand/';
		}
	?>
	<?php if($cats) : ?>
		<ul class="popular-category-list">
		<?php foreach($cats as $cat) : ?>
			<li><a href="/product-category/<?php echo $cat->slug; ?>"><?php echo $cat->name; ?></a></li>
		<?php endforeach; ?>	
		</ul>
		<a class="see-all" href="<?php echo $all_link; ?>"><?php the_sub_field('link_text');?> <i class="btr bt-arrow-right"></i></a>
	<?php endif; ?>
</section>