<section class="content-contact-info">
	<?php if($title = get_sub_field('contact_title')) : ?>
		<h3 class="contact-title">
			<?php if($link = get_sub_field('contact_title_link')) : ?>
				<a href="<?php the_sub_field('contact_title_link'); ?>"><?php echo $title; ?></a>
			<?php else : ?>
				<?php echo $title; ?>
			<?php endif; ?>
		</h3>
	<?php endif; ?>

	<?php if(have_rows('contact_fields')) : ?>
	<ul class="contact-list">
	<?php while(have_rows('contact_fields')) : the_row(); ?>
		<?php switch(get_sub_field('type')) : 
			case 'email': ?>
				<li class="contact-email">
					<span><a href="mailto:<?php the_sub_field('value'); ?>"><?php the_sub_field('value'); ?></a></span>
				</li>
				<?php break; ?>
			<?php case 'phone': ?>
				<li class="contact-phone">
					<span><a href="tel:+<?php the_sub_field('value'); ?>"><?php the_sub_field('value'); ?></a></span>
				</li>
				<?php break; ?>
			<?php case 'fax': ?>
				<li class="contact-fax">
					<span><?php the_sub_field('value'); ?></span>
				</li>
				<?php break; ?>
			<?php case 'address': ?>
				<li class="contact-address">
					<span><address><a target="_blank" href="//maps.google.com/?q=<?php echo urlencode(get_sub_field('value')); ?>"><?php the_sub_field('value'); ?></a></address></span>
				</li>
				<?php break; ?>
			<?php case 'link': ?>
				<li class="contact-link">
					<span><a target="_blank" href="<?php the_sub_field('value'); ?>"><?php the_sub_field('value'); ?></a></span>
				</li>
				<?php break; ?>
			<?php case 'map':  ?>
				<li>
					<div class="embed-container">
						<iframe frameborder="0" style="border:0" src="//maps.google.com/?q=<?php echo urlencode(get_sub_field('value')); ?>&amp;ie=UTF8&amp;output=embed" allowfullscreen></iframe>';
					</div>
				</li>
				<?php break; ?>
			<?php default: break; ?>
		<?php endswitch; ?>
	<?php endwhile; ?>
	</ul>
<?php endif; ?>
</section>