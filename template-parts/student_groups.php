<section class="content-team-grid content-student-groups">
	<h3 class="section-title"><?php echo get_sub_field('title'); ?></h3>
	<?php $grid_cols = get_sub_field('number_of_grid_columns'); ?>
	<?php $group_types = get_sub_field('group_type'); 
		
			if($group_types){ 

				  foreach($group_types as $term) {
					   $term = get_term($term);
						$groups = new WP_Query('post_type=student_groups&cohorts='.$term->slug.'&numberposts=-1');
						echo '<h3 class="group-title">'.$term->name.'</h3>';
						include(plugin_dir_path(__FILE__).'student_groups_loop.php');
						echo '<br><br>';
					}		
					
				} else {
					$args = array(
						'post_type' => 'student_groups',
						'posts_per_page' => -1,
					);
					
					$groups = new WP_Query($args);
					
					include(plugin_dir_path(__FILE__).'student_groups_loop.php');
					
				}
			
	?>
	
	
	
</section>

<script>
	jQuery(document).ready(function($){
		$('.bio-handle').on('click', function(e) {
			$(this).prev('.bio-wrap').slideToggle();
		});
	});
</script>