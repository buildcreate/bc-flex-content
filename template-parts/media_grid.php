<section class="content-media-grid">
		<?php $title = get_sub_field('title'); ?>
		<?php if($title) { ?>
			<h3 class="section-title"><?php echo $title; ?></h3>
		<?php
			}
			
		$image_ids = get_sub_field('gallery', false, false);
		$gal_type = get_sub_field('gallery_type');
		
		$shortcode = '[gallery type="'.$gal_type.'" link="file" ids="' . implode(',', $image_ids) . '"]';
		
		echo do_shortcode( $shortcode );
		
		?>

	<?php wp_reset_postdata(); ?>
</section>