<?php 
	$style = '';
	if($background = get_sub_field('background_color')){
		$style .= 'background:'.$background.';';
	}
	if($color = get_sub_field('text_color')){
		$style .= 'color:'.$color.';';
	}
?>
<section class="content-social-bar" style="<?php echo $style; ?>">
	<div class="wrapper">
		<?php the_sub_field('text'); ?>

		<?php if($rows = get_sub_field('social_icons')) : ?>
			<ul class="social-icon-list">
			<?php foreach($rows as $row) : ?>
				<li><a target="_blank" href="<?php echo $row['url']; ?>"><i class="fab fab-<?php echo $row['icon']; ?>"></i></a></li>
			<?php endforeach; ?>	
			</ul>
		<?php endif; ?>
	</div>
</section>