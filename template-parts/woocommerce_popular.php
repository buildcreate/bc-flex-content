<section class="content-woocommerce-popular">
	<?php $products = get_sub_field('products'); ?>
	<?php $num_cols = get_sub_field('grid_columns'); ?>
	<?php if($products) : ?>
		<ul class="popular-product-grid">
		<?php $woo_pf = new WC_Product_Factory(); ?>
		<?php foreach($products as $product) : ?>
			<li class="<?php echo grid_class($count, $num_cols); $count++; ?>">
				<div class="product-image">
					<a href="<?php echo get_the_permalink($product->ID); ?>">
						<?php echo get_the_post_thumbnail($product->ID, 'full'); ?>
					</a>
				</div>
				<div class="product-info">
					<a href="<?php echo get_the_permalink($product->ID); ?>">
						<?php echo $product->post_title; ?>
					</a>
					<div class="product-price">
						<?php $woo_product = $woo_pf->get_product($product->ID); ?>
						<?php echo $woo_product->get_price_html(); ?>
					</div>
				</div>
			</li>
		<?php endforeach; ?>
		</ul>
	<?php endif; ?>
</section>