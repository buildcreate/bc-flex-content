<section class="content-pricing-tiers">
	<div class="page-content">
		
		<div class="pricing-tiers">

		<?php if(have_rows('tiers')) { while(have_rows('tiers')) : the_row(); ?>
			<div class="tier <?php if(get_sub_field('featured')) { echo 'featured';} ?>">
				<div class="tier-title">
					<?php the_sub_field('title'); ?>
				</div>		
				<div class="tier-desc">
					<?php the_sub_field('subhead'); ?>
				</div>
				<?php if(have_rows('features')) { while(have_rows('features')) : the_row(); ?>
					<div class="tier-feature">
						<?php the_sub_field('title'); ?>
					</div>		
				<?php endwhile; } ?>
				<div class="tier-price">
					<?php the_sub_field('price'); ?>
				</div>
				<div class="tier-footer">
					<a href="<?php the_sub_field('button_url'); ?>" title="<?php the_sub_field('title'); ?>"><?php the_sub_field('button_text'); ?></a>
				</div>				
			</div>
		<?php endwhile; } ?>
		</div>
	</div>
</section>
