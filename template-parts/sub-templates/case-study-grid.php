<section class="content-case-study-display">	
	<h3 class="section-title"><?php echo get_sub_field('title'); ?></h3>
	<?php
		$grid_cols = get_sub_field('number_of_grid_columns');

		$case_cats = array();			
		if($case_study_categories = get_sub_field('case_study_categories')){ 
			foreach($case_study_categories as $cat){
				$case_cats[] = $cat->term_id;
			}
		}
	?>
	<?php 
		$args = array(
			'post_type' => 'case_study',
			'posts_per_page' => -1,
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'tax_query' => array(
				array(
					'taxonomy' => 'case_study_category',
					'field' => 'term_id',
					'terms' => $case_cats
				)
			)
		);
		$case_studies = new WP_Query($args);

		if($case_studies->have_posts()) : $count = 0; ?>
		
		<ul class="<?php if(get_sub_field('grid_type') == 'grid') {echo 'grid';} else {echo 'list';} ?>">
		<?php while($case_studies->have_posts()) : $case_studies->the_post(); ?>
			<li class="case-study <?php if(get_sub_field('grid_type') == 'grid') {echo grid_class($count, $grid_cols, 1); $count++; } ?>">
				
				<?php if(get_sub_field('grid_type') == 'grid') : ?>
					
					<div class="case-study-wrap">
						<a class="case-study-logo" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php the_field('logo'); ?>" alt="<?php the_title(); ?>" /></a>
						<h4 class="title">
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<?php the_title(); ?>
								<span><?php global $post; $terms = wp_get_post_terms($post->ID, 'case_study_category', array('fields' => 'names')); $i = 1; foreach( $terms as $t ) {echo $t; if($i<count($terms)) {echo ' | ';} $i++;} ?></span>
							</a>
						</h4>
					</div>
					
				<?php else : // listing ?>
					
					<?php if(has_post_thumbnail()) : ?>
						<?php the_post_thumbnail('thumbnail'); ?>
					<?php else : ?>
					
					<?php endif; ?>
					<div class="case-study-wrap">				
						<h3 class="title"><?php the_title(); ?></h3>
					</div>
				<?php endif; ?>
			</li>
		<?php endwhile; ?>
			<li class="clearer"></li>
		</ul>
	<?php endif; ?>
</section>
