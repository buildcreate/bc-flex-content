<?php
	
	$feature_size = get_sub_field('feature_size'); 
	
	if(get_sub_field('random_case_study')) {
		
		$args = array(
			'post_type' => 'case_study',
			'post_status' => 'publish',
			'posts_per_page' => 1,
			'orderby' => 'rand',	
		);
		
		$cs = get_posts($args)[0];
		
	} else {
		$cs = get_sub_field('featured_case_study');
	}
	
?>

<section class="content-featured-case-study <?php echo 'featured-case-study-'.$feature_size; ?>">
	
	<?php if($feature_size == 'large') { ?>
		
		<div class="left">
			<span class="case-study-title">
				<?php echo get_sub_field('title'); ?>
			</span>
			
			<div class="case-study-summary">
				<?php echo get_field('summary', $cs->ID); ?>
			</div>
			
			<a class="button case-study-permalink" href="<?php echo get_permalink($cs->ID); ?>" title="<?php echo $cs->post_title; ?>">
				Read More
			</a>
			
			<a class="button all-case-studies" href="<?php echo get_field('case_studies_page'); ?>" title="All Case Studies">
				All Case Studies
			</a>		
		</div>
		
		<div class="right">
			
			<img class="featured-case-study-logo" src="<?php echo get_field('logo', $cs->ID); ?>" alt="<?php echo $cs->post_title; ?>" />
			
			<blockquote>
				<?php echo get_post_meta($cs->ID, 'quote', true); ?>
			</blockquote>
			
			<cite>
				<?php echo get_post_meta($cs->ID, 'cite', true); ?><br>
				<strong><?php echo $cs->post_title; ?></strong>
			</cite>
			
			
		</div>
		
	<?php } if($feature_size == 'compact') { ?>
		<div class="left">
			<img class="featured-case-study-logo" src="<?php echo get_field('logo', $cs->ID); ?>" alt="<?php echo $cs->post_title; ?>" />
			<a class="button case-study-permalink" href="<?php echo get_permalink($cs->ID); ?>" title="<?php echo $cs->post_title; ?>">
				Read Case Study
			</a>			
		</div>		
		<div class="right">			
			<div class="case-study-summary">
				<?php echo get_field('summary', $cs->ID); ?>
			</div>		
		</div>	
	<?php } ?>
	
</section>