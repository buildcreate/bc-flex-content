<?php $bio_link = get_sub_field('staff_bio_link'); ?>
<?php $rand_id = rand(1,9999); ?>
<section id="team-grid-<?php echo $rand_id; ?>" class="content-team-grid">
	<h3 class="section-title"><?php echo get_sub_field('title'); ?></h3>
	<?php $grid_cols = get_sub_field('number_of_grid_columns'); ?>
	<?php $team_roles = get_sub_field('team_roles'); 
			$bcflex_options = get_option('flex_settings');
			
			if($team_roles){ 
					$tax_query = array(
						array(
							'taxonomy' => 'staff_type',
							'field' => 'ids',
							'terms' => $team_roles
						)
					);			
					
				} else {
					$tax_query = array();
				}
		
	?>
	<?php 
		$args = array(
			'post_type' => 'staff',
			'posts_per_page' => -1,
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'tax_query' => $tax_query
		);
		$team = new WP_Query($args);
	?>
	<?php if($team->have_posts()) : ?>
		<?php $count = 0; ?>
		<ul class="<?php if(get_sub_field('grid_type') == 'featured') {echo 'grid';} else {echo 'list';} ?>">
		<?php while($team->have_posts()) : $team->the_post(); ?>
			<li class="team-member <?php if(get_sub_field('grid_type') == 'featured') {echo grid_class($count, $grid_cols, 1); $count++; } ?>">
				
				<?php if(get_sub_field('grid_type') == 'featured') : ?>
				
					<?php if(has_post_thumbnail()) : ?>
						<?php the_post_thumbnail('full'); ?>
					<?php else : ?>
					
					<?php endif; ?>
					
					<div class="bio-wrap">
						<h4 class="name"><?php the_title(); ?></h4>
						<div class="title"><?php echo get_field('title'); ?></div>
						<div class="email"><a target="_blank" href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></div>
						<?php if(get_the_content()) : ?>
							<div class="bio-handle">
								<?php if($bio_link) : ?>
									<a href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title(); ?>">Read Bio</a>
								<?php else : ?>
									Bio
								<?php endif; ?>
							</div>
						<?php endif; ?>
						<div class="bio user-content"><h3><?php the_title(); ?></h3><?php the_content(); ?></div>
					</div>
				<?php else : // listing ?>
		
					<?php if(has_post_thumbnail()) : ?>
						<?php the_post_thumbnail('thumbnail'); ?>
					<?php else : ?>
					
					<?php endif; ?>
					<div class="bio-wrap">				
						<h3 class="name"><?php the_title(); ?></h3>
						<div class="title"><?php echo get_field('title'); ?></div>
						<div class="email"><a target="_blank" href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></div>
						<div class="bio user-content"><?php the_content(); ?></div>
					</div>
				<?php endif; ?>
			</li>
		<?php endwhile; ?>
			<li class="clearer"></li>
		</ul>
	<?php endif; ?>
</section>

<?php if(!$bio_link) { ?>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/js/jquery.tooltipster.min.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo plugin_dir_url(__FILE__); ?>../css/tooltipster.css" />
	<script>
		jQuery(document).ready(function($){
			$('#team-grid-<?php echo $rand_id; ?> .bio-handle').tooltipster({
				contentAsHTML  : true, 
				maxWidth       : 500,  
				 
				functionInit: function(){
				 	return $(this).next('.bio').html();
			    },
			    functionReady: function(){
			        $(this).next('.bio').attr('aria-hidden', false);
			    },
			    functionAfter: function(){
			        $(this).next('.bio').attr('aria-hidden', true);
			    }
			});			
		});
	</script>

<?php } ?>