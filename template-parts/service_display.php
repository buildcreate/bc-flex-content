<section class="content-services-display">
	<h3 class="section-title"><?php echo get_sub_field('title'); ?></h3>
	<?php 
		$grid_cols = get_sub_field('number_of_grid_columns');	

		$cat_arr = array();
		if($service_categories = get_sub_field('service_categories')){ 
			
			foreach($service_categories as $cat){
				$cat_arr[] = $cat->term_id;
			}
		}	
			
		$args = array(
			'post_type' => 'service',
			'posts_per_page' => -1,
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'tax_query' => array(
				array(
					'taxonomy' => 'service_category',
					'field' => 'term_id',
					'terms' => $cat_arr
				)
			)
		);
		$services = new WP_Query($args);
	?>

	<?php if($services->have_posts()) : ?>
		<?php $count = 0; ?>
		<ul class="<?php if(get_sub_field('grid_type') == 'grid') {echo 'grid';} else {echo 'list';} ?>">
		<?php while($services->have_posts()) : $services->the_post(); ?>
			<li class="service <?php if(get_sub_field('grid_type') == 'grid') {echo grid_class($count, $grid_cols, 1); $count++; } ?>">
				
				<?php if(get_sub_field('grid_type') == 'grid') : ?>
				
					<?php if(has_post_thumbnail()) : ?>
						
						<?php the_post_thumbnail('medium'); ?>
						<a class="service-wrap" href="<?php the_permalink(); ?>">
							<h4 class="title"><?php the_title(); ?></h4>							
						</a>	
						
					<?php else : ?>
						<a class="service-wrap" href="<?php the_permalink(); ?>">
							<h4 class="title"><?php the_title(); ?></h4>							
						</a>
					<?php endif; ?>
					
				<?php else : // listing ?>
					
					<?php if(has_post_thumbnail()) : ?>
						<?php the_post_thumbnail('small'); ?>
					<?php else : ?>
					
					<?php endif; ?>
					<div class="service-wrap">				
						<h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					</div>
				<?php endif; ?>
			</li>
		<?php endwhile; ?>
			<li class="clearer"></li>
		</ul>
	<?php endif; ?>
</section>
