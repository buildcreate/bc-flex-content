<section class="content-woocommerce-featured">
	<?php $products = get_sub_field('products'); ?>

	<?php if($products) : ?>
	<ul class="woo-featured-grid">
		<?php $count = 0; ?>
		<?php foreach($products as $product_id) : ?>
			<li class="<?php echo grid_class($count, 4); ?>">
				<?php // get the category ?>
				<?php $term_id = false; ?>
				<?php $terms = wp_get_post_terms($product_id, 'product_cat'); ?>
				<?php if($terms) : ?>
					<?php foreach($terms as $term) : ?>
						<?php $term_id = $term->term_id; ?>
						<?php break; ?>
					<?php endforeach; ?>
				<?php endif; ?>
				<div class="product-image">
					<a href="/product-category/<?php echo $term->slug; ?>">
						<?php echo get_the_post_thumbnail($product_id, 'full'); ?>
					</a>
				</div> 
				<div class="product-category" style="background:<?php the_sub_field('category_background_color'); ?>;">
					<a href="/product-category/<?php echo $term->slug; ?>" style="color:<?php the_sub_field('category_text_color'); ?>;"><?php echo $term->name; ?></a>
				</div>
				<?php if($term_id) : ?>
					<div class="category-brands">
						<?php 
							// get products that have this cat
							$args = array(
								'post_type' => 'product',
								'posts_per_page' => -1,
								'tax_query' => array(
									array(
										'taxonomy' => 'product_cat',
										'terms' => $term_id,
										'field' => 'term_id'
									)
								)
							);
							$products_with_term = new WP_Query($args);

							// loop through them all and build a brands array
							$brands_arr = array();
							if($products_with_term->posts){
								foreach($products_with_term->posts as $product) {
									$brands = wp_get_post_terms($product->ID, 'product_brand'); 
									if($brands){
										foreach($brands as $brand){
											if(!array_key_exists($brand->term_id, $brands_arr)){
												$brands_arr[$brand->term_id] = $brand;
											}
										}
									}
								}
							}	
						?>
						<?php if($brands_arr) : ?>
							<ul class="category-brands-list">
								<?php foreach($brands_arr as $brand) : ?>
									<li><a href="/brand/<?php echo $brand->slug; ?>"><?php echo $brand->name; ?></a></li>
								<?php endforeach; ?>
							</ul>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</li>
			<?php $count++; ?>
		<?php endforeach; ?>
	</ul>
	<?php endif; ?>
</section>