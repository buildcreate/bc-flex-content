<section class="content-image" style="text-align:<?php the_sub_field('image_alignment'); ?>;">
	<?php 
		$img = get_sub_field('image');
		$shape = get_sub_field('shape'); if($shape) { ?>
		<div class="<?php echo strtolower($shape); echo strtolower(get_sub_field('triangle_direction')); ?>">
	<?php } ?>
			<?php if(!$shape) { ?>
				<img src="<?php echo $img['url']; ?>" />
			<?php } else { ?>
				<img src="<?php echo $img['sizes']['image_shape']; ?>" height="<?php echo $img['sizes']['image_shape-width']; ?> " />
			<?php } ?>
	<?php if($shape) { ?>
	</div>
	<?php } ?>
</section>