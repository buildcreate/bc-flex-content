<section class="content-custom-block">
	<?php 		
		$fields_arr = array('rand_id' => rand(0,1000));
		$fields = get_sub_field('fields');
		if($fields){
			foreach($fields as $field){
				$fields_arr[$field['key']] = $field['value'];
			}
		}
		do_action(get_sub_field('hook'), $fields_arr); 
	?>
</section>