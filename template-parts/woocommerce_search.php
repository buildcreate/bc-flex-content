<section class="content-product-search">
	<?php 
		// do search
		$query = $_GET["q"] ? sanitize_text_field($_GET["q"]) : "";
		$page = $_GET["pg"] ? sanitize_text_field($_GET["pg"]) : 1;
		$sort = $_GET["sort"] ? sanitize_text_field($_GET["sort"]) : false;
		$order = $_GET["order"] ? sanitize_text_field($_GET["order"]) : false;
		$min = $_GET["min"] ? sanitize_text_field($_GET["min"]) : 0;
		$max = $_GET["max"] ? sanitize_text_field($_GET["max"]) : false;
		$brands = $_GET["brand"];
		$cats = $_GET["cat"];
		$ppp = 12;

		// starting search arguments
		$args = array(	
			'post_type' => 'product',
			'posts_per_page' => $ppp,
			'paged' => $page,
			's' => $query,
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => '_price',
					'value' => $min,
					'compare' => '>=',
					'type' => 'NUMERIC'
				)
			),
			'tax_query' => array(
				'relation' => 'OR'
			)
		);

		// sort
		if($sort == 'name' && $order){
			$args['orderby'] = 'title';
			$args['order'] = $order;
		}elseif($sort == 'price' && $order){
			$args['orderby'] = 'meta_value_num';
			$args['order'] = $order;
			$args['meta_key'] = '_price';
		}

		// filter
		if($max !== false){
			$args['meta_query'][] = array(
				'key' => '_price',
				'value' => $max,
				'compare' => '<=',
				'type' => 'NUMERIC'
			);
		}

		if($brands){
			$args['tax_query'][] = array(
				'taxonomy' => 'product_brand',
				'field' => 'term_id',
				'terms' => $brands
			);
		}			

		if($cats){
			$args['tax_query'][] = array(
				'taxonomy' => 'product_cat',
				'field' => 'term_id',
				'terms' => $cats
			);
		}

		// get results
		$results = new WP_Query($args);
		$num_results = $results->found_posts;
	?>
	<h4 class="number-of-results"><?php echo $num_results; ?> results found for "<?php echo $query; ?>"</h4>
	
	<div class="product-search-sidebar">
		<form class="sidebar-search-form" method="GET" action="">
			<div class="sidebar-header search-wrap">
				<div class="search-container">
					<input type="text" name="q" value="<?php echo $query; ?>" />
					<button name="test" value="1"><i class="btb bt-search"></i></button>
				</div>
			</div>
			<?php 
				// get results without filters
				$args = array(	
					'post_type' => 'product',
					'posts_per_page' => -1,
					's' => $query,
				);			

				// get results
				$no_filter_results = new WP_Query($args);
				$price_max = $max ? intval($max) : 0;
				if($no_filter_results->found_posts){
					foreach($no_filter_results->posts as $product){
						$price = intval(get_post_meta($product->ID, '_price', true));
						if($price > $price_max){ 
							$price_max = $price;
						}
					}
				}
			?>
			<div class="sidebar-section search-refine">
				<h6>Refine</h6>
				<div class="refine-container">
					<div id="refine-slider"></div>
					<div class="refine-values">
						<strong>Price:</strong> $<span class="refine-min"><?php echo $min ? $min : 0; ?></span> - $<span class="refine-max"><?php echo $max ? $max : $price_max; ?></span>
					</div>
					<input id="refine-min" name="min" type="hidden" value="<?php echo $min ? $min : 0; ?>" />
					<input id="refine-max" name="max" type="hidden" value="<?php echo $max ? $max : $price_max; ?>" />
					<input type="submit" value="Update">
				</div>
			</div>	

			<script type="text/javascript">
				jQuery(document).ready(function($){
					$("#refine-slider").slider({
						range: true,
						min: 0,
						max: <?php echo $price_max; ?>,
						values: [<?php echo $min ? $min : 0; ?>, <?php echo $max ? $max : $price_max; ?>],
						slide: function(event, ui){
							$('#refine-min').val(ui.values[0]);
							$('.refine-min').text(ui.values[0]);
							$('#refine-max').val(ui.values[1]);
							$('.refine-max').text(ui.values[1]);
						}
					});
				});
			</script>

			<?php 
				// get brands and cats
				$brands_arr = array();
				$cats_arr = array(); 
				if($no_filter_results->found_posts){
					foreach($no_filter_results->posts as $product){
						$product_brands = wp_get_post_terms($product->ID, 'product_brand');
						if($product_brands){
							foreach($product_brands as $brand){
								if(!array_key_exists($brand->term_id, $brands_arr)){
									$brands_arr[$brand->term_id] = $brand;
								}
							}
						}							
						$product_cats = wp_get_post_terms($product->ID, 'product_cat');
						if($product_cats){
							foreach($product_cats as $cat){
								if(!array_key_exists($cat->term_id, $cats_arr)){
									$cats_arr[$cat->term_id] = $cat;
								}
							}
						}
					}
				}
			?>

			<?php if($cats_arr) : ?>
				<div class="sidebar-header">Categories</div>
				<div class="sidebar-section">
					<ul class="checkbox-list cat-list">
						<?php foreach($cats_arr as $cat) : ?>
							<li>
								<input id="cat-<?php echo $cat->term_id; ?>" type="checkbox" name="cat[]" value="<?php echo $cat->term_id; ?>" <?php if(is_array($cats) && in_array($cat->term_id, $cats)){echo 'checked="checked"';} ?>>
								<label for="cat-<?php echo $cat->term_id; ?>"><?php echo $cat->name; ?></label>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>

			<?php if(is_taxonomy('product_brand') && $brands_arr) : ?>
				<div class="sidebar-header">Brands</div>
				<div class="sidebar-section">
					<ul class="checkbox-list brand-list">
						<?php foreach($brands_arr as $brand) : ?>
							<li>
								<input id="brand-<?php echo $brand->term_id; ?>" type="checkbox" name="brand[]" value="<?php echo $brand->term_id; ?>" <?php if(is_array($brands) && in_array($brand->term_id, $brands)){echo 'checked="checked"';} ?>> 
								<label for="brand-<?php echo $brand->term_id; ?>"><?php echo $brand->name; ?></label>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>		
			<?php endif; ?>	

			<script type="text/javascript">
				jQuery(document).ready(function($){
					$('.checkbox-list input').on('change', function(){
						$(this).closest('form').submit();
					});
				});
			</script>

			<input type="hidden" name="sort" value="<?php echo $sort; ?>" />
			<input type="hidden" name="order" value="<?php echo $order; ?>" />
		</form>
	</div>
	<div class="product-search-results">
		<div class="results-top-bar">
			<span class="showing-results">page <?php echo $page; ?> of <?php echo ceil($num_results / $ppp); ?></span> 
			<span class="sort-by">SORT BY
				<span class="sort-by-price">
					<?php if($order == 'asc' && $sort == 'price') : ?>
						<a href="?<?php echo http_build_query(array_merge($_GET, array("sort" => "price", "order" => "desc"))); ?>">Price <i class="bts bt-caret-up"></i></a>
					<?php else : ?>
						<a href="?<?php echo http_build_query(array_merge($_GET, array("sort" => "price", "order" => "asc"))); ?>">Price 
							<?php if($sort == 'price') : ?>
								<i class="bts bt-caret-down"></i>
							<?php endif; ?>
						</a>
					<?php endif; ?>
				</span>
				<span class="sort-by-name">
					<?php if($order == 'asc' && $sort == 'name') : ?>
						<a href="?<?php echo http_build_query(array_merge($_GET, array("sort" => "name", "order" => "desc"))); ?>">Name <i class="bts bt-caret-up"></i></a>
					<?php else : ?>
						<a href="?<?php echo http_build_query(array_merge($_GET, array("sort" => "name", "order" => "asc"))); ?>">Name 
							<?php if($sort == 'name') : ?>
								<i class="bts bt-caret-down"></i>
							<?php endif; ?>
						</a>
					<?php endif; ?>
				</span>
			</span>
		</div>

		<?php if($num_results) : ?>
		<?php global $product; ?>
		<ul class="results-list">
			<?php $count = 0; ?>
			<?php foreach($results->posts as $result) : ?>
				<li class="<?php echo grid_class($count, 3); $count++; ?>">
					<a href="<?php echo get_permalink($result->ID); ?>">
						<div class="product-image">
							<?php echo get_the_post_thumbnail($result->ID, 'full'); ?>
						</div>
						<div class="product-info">
							<?php 
								echo $result->post_title;
								$product = wc_get_product($result->ID);
								woocommerce_template_single_price();
							?>
						</div>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>

		<?php $num_pages = ceil($num_results / $ppp); ?>
		<?php if($num_pages > 1) : ?>
			<div class="results-pagination">
				<?php if($page > 1) : ?>
					<div class="pagination-previous"><a href="/product-search/?<?php echo http_build_query(array_merge($_GET, array("pg" => $page - 1)));?>"><i class="btr bt-angle-left"></i> Previous</a></div>
				<?php endif; ?>					

				<?php if($num_pages > $page) : ?>
					<div class="pagination-next"><a href="/product-search/?<?php echo http_build_query(array_merge($_GET, array("pg" => $page + 1)));?>">Next <i class="btr bt-angle-right"></i></a></div>
				<?php endif; ?>

				<ul class="pagination-list">
				<?php for($i = 1; $i <= $num_pages; $i++) : ?>
					<li>
						<?php if($i == $page) : ?>
							<strong><?php echo $page; ?></strong>
						<?php else : ?>
							<a href="/product-search/?<?php echo http_build_query(array_merge($_GET, array("pg" => $i)));?>"><?php echo $i; ?></a>
						<?php endif; ?>
					</li>
				<?php endfor; ?>
				</ul>
			</div>
		<?php endif; ?>
	</div>
</section>