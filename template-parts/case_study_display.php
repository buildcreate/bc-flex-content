<?php 

	if(get_sub_field('display_type') == 'grid') {
		include('sub-templates/case-study-grid.php');
	} 
	
	if(get_sub_field('display_type') == 'featured') {
		include('sub-templates/featured-case-study.php');
	}
	
?>