<?php 
	// get layouts in this row
	if(have_rows('nested_row_layout')){
		while(have_rows('nested_row_layout')){
			the_row();

			// get block settings
			$padding_top = get_sub_field('padding_top') ? get_sub_field('padding_top'). 'px' : 0 . 'px';
			$padding_bottom = get_sub_field('padding_bottom') ? get_sub_field('padding_bottom'). 'px' : 0 . 'px';
			$padding_left = get_sub_field('padding_left') ? get_sub_field('padding_left'). 'px' : 0 . 'px';
			$padding_right = get_sub_field('padding_right') ? get_sub_field('padding_right'). 'px' : 0 . 'px';
			
			$block_width = get_sub_field('block_width');
			
			$mobile_class = get_sub_field('hide_on_mobile') ? 'mobile-hide' : '';
			$flex_vc = get_sub_field('vertical_center') ? 'flex-vc' : '';
			
			$block_class = $mobile_class.' '.$flex_vc;

				?>
					<div class="content-block <?php echo apply_filters('bc_flex_block_class', $block_class); ?>" style="width:<?php echo $block_width; ?>%;padding:<?php echo $padding_top . ' ' . $padding_right . ' ' . $padding_bottom . ' ' . $padding_left; ?>;">
				<?php

			include(plugin_dir_path(__FILE__) . get_row_layout() . '.php');

			if(!is_home() && !is_category() && !is_archive()) {
				wp_reset_postdata();
			}
			
			?>
				</div>
			<?php
					
		}
	}
?>