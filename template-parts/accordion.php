<section class="content-accordion content-block">
	<?php $title_text = get_sub_field('title_text_color'); ?>
	<?php $title_bg = get_sub_field('title_background_color'); ?>
	<?php $title_opacity = get_sub_field('title_background_opacity'); ?>
	<?php $content_text = get_sub_field('content_text_color'); ?>
	<?php $content_bg = get_sub_field('content_background_color'); ?>
	<?php $content_opacity = get_sub_field('content_background_opacity'); ?>

	<style type="text/css">
		
		<?php if($title_text) { ?>
		
			.accordion .accordion-title {
				color:<?php echo $title_text; ?> !important;
				border-bottom:1px solid <?php echo $title_text; ?> !important;
			}
		
		<?php } ?>
		
		<?php if($title_bg && $title_opacity) { ?>
		
			.accordion .accordion-title {
				background:rgba(<?php echo hexdec(substr($title_bg,1,2)); ?>, <?php echo hexdec(substr($title_bg,3,2)); ?>, <?php echo hexdec(substr($title_bg,5,2)); ?>, <?php echo $title_opacity; ?>) !important;				
			}
		
		<?php } ?>		
		
		<?php if($content_bg && $content_opacity) { ?>
		
			.accordion .accordion-content{
				background:rgba(<?php echo hexdec(substr($content_bg,1,2)); ?>, <?php echo hexdec(substr($content_bg,3,2)); ?>, <?php echo hexdec(substr($content_bg,5,2)); ?>, <?php echo $content_opacity; ?>) !important;
			}
			
		<?php 
			
			}
			
			if($content_text) { ?>
			
			.accordion .accordion-content p,
			.accordion .accordion-content ul,
			.accordion .accordion-content li,
			.accordion .accordion-content h1,
			.accordion .accordion-content h2,
			.accordion .accordion-content h3,
			.accordion .accordion-content h4,
			.accordion .accordion-content h5 {
				 color:<?php echo $content_text; ?> !important;
			}
			
		<?php } ?>
	</style>

	<h3 class="section-title"><?php echo get_sub_field('title'); ?></h3>
	<?php $type = get_sub_field('type_of_content'); ?>
	<?php if($type == 'custom_content') : ?>
		<?php if(have_rows('content')) : ?>
		
			<ul class="accordion">
				<?php while(have_rows('content')) : the_row(); $ac_content = get_sub_field('content'); ?>
					<li class="accordion-item">
						<h4 class="accordion-title <?php if($ac_content) {echo 'has-content';} ?>" ><i class="btr bt-angle-down"></i><?php the_sub_field('title'); ?></h4>
						<div class="user-content accordion-content page-content" style="display:none;">
							<?php echo $ac_content; ?>
						</div>
					</li>
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>
	
	<?php elseif($type == 'content_type') : ?>
		
		<?php $accordion_content = get_sub_field('content_type'); ?>
		<?php if($accordion_content) : ?>
	
			<ul class="accordion">
				<?php foreach($accordion_content as $a) : ?>
					<li class="accordion-item">
						<h4 class="accordion-title has-content"><i class="btr bt-angle-down"></i><?php echo $a->post_title; ?></h4>
						<div class="user-content accordion-content" style="display:none;"><?php echo wpautop($a->post_content); ?></div>
					</li>
				<?php endforeach; ?>
			</ul>
		
		<?php endif; ?>
	<?php endif; ?>	
</section>