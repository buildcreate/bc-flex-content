<?php 
	$button_style = '';
	$button_width = get_sub_field('button_width');
	$button_text_alignment = get_sub_field('button_text_alignment');
	$button_alignment = get_sub_field('button_alignment');
	$layout = get_sub_field('button_layout');
	$stack_class = "";
	$stack_class .= $layout == 'inline' ? ' inline' : ' stack';
	switch ($button_alignment) {
		case 'left': $stack_class .= ' left'; break;
		case 'center': $stack_class .= ' center'; break;
		case 'right': $stack_class .= ' right'; break;
		default: break;
	}
?>
<section class="content-button-stack">	
	<?php // get buttons ?>
	<?php if(have_rows('buttons')) : ?>
		<ul class="button-stack<?php echo $stack_class; ?>">
		<?php while(have_rows('buttons')) : the_row(); ?>
			<li style="width:<?php echo $button_width; ?>">
				<?php 
					if($bg_color = get_sub_field('background_color')){$button_style .= 'background:'.$bg_color.';';}
					if($text_color = get_sub_field('text_color')){$button_style .= 'color:'.$text_color.';';}
					if($border_color = get_sub_field('border_color')){$button_style .= 'border:1px solid '.$border_color.';';}
					if($border_radius = get_sub_field('border_radius')){$button_style .= 'border-radius:'.$border_radius.'px;';}
				?>
				<a class="stack-button <?php echo $button_text_alignment; ?>" href="<?php the_sub_field('link'); ?>" style="<?php echo $button_style; ?>">
					<?php echo the_sub_field('text'); ?>
				</a>
			</li>
		<?php endwhile; ?>
		</ul>
	<?php endif; ?>
</section>