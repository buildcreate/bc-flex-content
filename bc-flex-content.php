<?php
	/*
		Plugin Name:			BC Flex Content
		Plugin URI:				http://www.buildcreate.com/
		Description:			Custom Flex Content Interface based on ACF Pro
		Version:				1.7.11
		Author:					buildcreate, a2rocklobster
		Author URI:				http://www.buildcreate.com/
		Copyright:				buildcreate
		Bitbucket Plugin URI:	http://bitbucket.org/buildcreatestudios/bc-flex-content
	*/

	if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

	define("BCFLEXPATH", plugin_dir_path(__FILE__));
	define('ACFGFS_API_KEY', 'AIzaSyCpMTxBPpWOfxNKe6UglMlcMsqNeFSt7mQ');

	class BC_Flex_Content {

		// add styles and scripts to ACF
		
		function __construct() {
			
			// Enqueue our scripts and styles
			add_action('acf/input/admin_enqueue_scripts', array( $this, 'acf_page_builder_enqueue'));
			add_action('wp_enqueue_scripts', array($this, 'add_front_styles'));
			add_action('wp_enqueue_scripts', array($this, 'front_js'));
			add_action('admin_footer', array($this, 'remove_acf_fields'));
			
			// remove flexible content "no content" messge
			add_filter( 'acf/fields/flexible_content/no_value_message', function($message, $field){return '';}, 10, 2);
			
			// Hook into content filter to add our layouts
			add_filter('the_content', array($this, 'add_layouts_to_content'));

	      	// Add GoogleMaps API Key     
			add_filter('acf/fields/google_map/api', function($api){
				$api['key'] = 'AIzaSyAE8KM7pt3Px9MQdzoAx_quKShVf5rOCzg';
			  	return $api;
			});
			
			add_action('after_setup_theme', array($this, 'image_shape_size'));

		}	
		
		function image_shape_size() {
			add_image_size('image_shape', 500, 500, true);
		}
		
		function acf_page_builder_enqueue(){	
			// acf stylesheet on admin
			wp_enqueue_style( 'acf-page-builder', plugin_dir_url(__FILE__) . '/css/acf.css' ); 
			wp_enqueue_style( 'acf-page-builder-blacktie', plugin_dir_url(__FILE__) . '/css/black-tie.css' ); 		
			// acf page builder js on admin
			wp_enqueue_script( 'acf-page-builder', plugin_dir_url(__FILE__) . '/js/acf-page-builder.js', true );
			//wp_enqueue_script( 'acf-rangeslider', plugin_dir_url(__FILE__) . '/js/nouislider.min.js', true );
		}
		
		function front_js() {
			wp_enqueue_script( 'js-hashchange', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-hashchange/1.3/jquery.ba-hashchange.min.js', true );
			wp_enqueue_script( 'easytabs', plugin_dir_url(__FILE__) . '/js/jquery.easytabs.min.js', true );
			wp_enqueue_script( 'bc-flex-front', plugin_dir_url(__FILE__) . '/js/front.js', true );
		}
		
		function add_front_styles() {
			wp_enqueue_style( 'acf-page-builder-blacktie', plugin_dir_url(__FILE__) . '/css/black-tie.css' );
			wp_enqueue_style( 'acf-page-builder-layouts-dep', plugin_dir_url(__FILE__) . '/css/layouts-dep.css' ); 
			wp_enqueue_style( 'acf-page-builder-layouts', plugin_dir_url(__FILE__) . '/css/layouts.css' ); 
			wp_enqueue_style( 'acf-rangeslider', plugin_dir_url(__FILE__) . '/css/nouislider.css' ); 

			wp_register_script('googlemaps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAE8KM7pt3Px9MQdzoAx_quKShVf5rOCzg',null,null,true);  
			wp_enqueue_script('googlemaps');		
			
		}

		function remove_acf_fields() {
		
			$output = '<script>jQuery(document).ready(function($){$(document).on("click", "a[data-event=\'add-layout\']", function(e) {';
					
			if (!get_field('enable_faqs', 'option')) {
				$output .= '$("a[data-layout=\'faqs\']").remove();';
			}
			
			if (!get_field('enable_staff', 'option')) {
				$output .= '$("a[data-layout=\'team_grid\']").remove();';
			}
			
			if (!get_field('enable_resources', 'option')) { 
				$output .= '$("a[data-layout=\'resources_grid\']").remove();';
			}
			
			if (!get_field('enable_case_studies', 'option')) {
				$output .= '$("a[data-layout=\'case_study_display\']").remove();';
			}
			
			if (!get_field('enable_services', 'option')) {
				$output .= '$("a[data-layout=\'services_display\']").remove();';
			}
			
			if (!get_field('enable_member_groups', 'option')) {
				$output .= '$("a[data-layout=\'student_groups\']").remove();';
			}				

			if (!get_field('enable_woocommerce', 'option')) {
				$output .= '$("a[data-layout=\'woocommerce_featured\']").remove();';
				$output .= '$("a[data-layout=\'woocommerce_popular\']").remove();';
				$output .= '$("a[data-layout=\'woocommerce_search\']").remove();';
				$output .= '$("a[data-layout=\'woocommerce_categories\']").remove();';
			}	
			
			if (!class_exists('Nessie')) {
				$output .= '$("a[data-layout=\'upcoming_events_list\']").remove();';
			}	
			
			if (!class_exists('FundingSerious')) {
				$output .= '$("a[data-layout=\'funding_serious\']").remove();';
			}	
			
			if (!class_exists('CollegiatePoints')) {
				$output .= '$("a[data-layout=\'member_points\']").remove();';
			}
			
			if (!class_exists('Jetpack')) {
				$output .= '$("a[data-layout=\'facebook_widget\']").remove();';
			}		
			
			$output .= 'height = $(".acf-fc-popup").height(); $(".acf-fc-popup").css("margin-top", -height - 40);';
			
			$output .= '}); });</script>';
			
			echo $output;
			
		}
		
		function build_sidebar() {
			if(is_page() && get_field('use_sidebar')) {
				ob_start();
				include(plugin_dir_path(__FILE__).'sidebar.php');
				$sidebar = ob_get_clean();
				
				return $sidebar;
			}

			return false;
			
		}
		
		function add_layouts_to_content($content) {
			if(have_rows('rows') && !post_password_required()) {
				$option = '';
				ob_start();
				include(plugin_dir_path(__FILE__).'layouts.php');
				$layouts = ob_get_clean();
				
				$sidebar = $this->build_sidebar();
				
				if($sidebar) {			
					return $layouts.$sidebar;
				} else {
					return $layouts;
				}

			} else {			
				return $content;
			}
		}
		
		function add_layouts_to_footer() {
			if(have_rows('rows', 'option')) {
				$option = 'option';
				ob_start();
				include(plugin_dir_path(__FILE__).'layouts.php');
				$layouts = ob_get_clean();
				
				echo $layouts;
			}
		}

	}

	include_once(plugin_dir_path(__FILE__) . 'inc/twitter-widget/wp-latest-twitter-tweets.php');
	add_action('acf/init', 'bc_flex_add_field_groups');
	function bc_flex_add_field_groups() {	
		
		// Custom fields for custom post types in the site options
		include_once(plugin_dir_path(__FILE__) . 'defs/site_options.php');		

		// Custom post types selection in site options
		include_once(plugin_dir_path(__FILE__) . 'defs/content-row-options.php');
		
		// Custom fields for custom post types
		include_once(plugin_dir_path(__FILE__) . 'defs/custom_fields.php');

		// Page Content Rows
		$json = trim(file_get_contents(plugin_dir_path(__FILE__) . 'defs/content-rows.json'), '[]');
		$json_arr = json_decode($json, true);

		// maybe modify which post types content rows display on
		$post_types = get_field('post_types_using_content_rows', 'option');
		if($post_types){

			// add in the selected types (duplicates are fine)
			foreach($post_types as $type){
				$json_arr['location'][] = array(
					array(
						'param' => 'post_type',
						'operator' => '==',
						'value' => $type,
					)
				);
			}
		}

		// add content rows
		if(function_exists('acf_add_local_field_group')){
			acf_add_local_field_group($json_arr);
		}
	}

	add_action('acf/include_field_types', 'bc_flex_content_fields');
	function bc_flex_content_fields() {

		// Include our custom fields and gravity form integration	
		include_once(plugin_dir_path(__FILE__) . 'inc/acf-nav-field/nav-menu-v5.php');
		include_once(plugin_dir_path(__FILE__) . 'inc/acf-gravity_forms.php');
		include_once(plugin_dir_path(__FILE__) . 'inc/acf-sidebar_selector-v5.php');
		include_once(plugin_dir_path(__FILE__) . 'inc/acf-widget-field/acf-a_widget.php');
		include_once(plugin_dir_path(__FILE__) . 'inc/acf-post-type-field/post-type-selector-v5.php');
		include_once(plugin_dir_path(__FILE__) . 'inc/acf-google-font/functions.php');
		include_once(plugin_dir_path(__FILE__) . 'inc/acf-google-font/acf-google_font_selector-v5.php');
	}

	if(function_exists('acf_add_options_page')) { 
		acf_add_options_page(array(
			'page_title' 	=> 'Site Options',
			'menu_title' 	=> 'Site Options',
			'menu_slug' 	=> 'site-options',
			'capability' 	=> 'edit_posts',
			'redirect' 	=> false
		));
	}

	// FontAwesome TinyMCE stuff

	include(plugin_dir_path(__FILE__).'inc/bfa-lib/better-font-awesome-library.php');
	include(plugin_dir_path(__FILE__).'inc/tinymce-tables/mce_table_buttons.php');




	add_action( 'init', 'bc_flex_load_');

	/**
	 * Initialize the Better Font Awesome Library.
	 *
	 * (see usage notes below on proper hook priority)
	 */
	function bc_flex_load_() {

	    // Set the library initialization args (defaults shown).
	    $args = array(
	            'version'             => 'latest',
	            'minified'            => true,
	            'remove_existing_fa'  => false,
	            'load_styles'         => true,
	            'load_admin_styles'   => true,
	            'load_shortcode'      => true,
	            'load_tinymce_plugin' => true,
	    );

	    // Initialize the Better Font Awesome Library.
	    Better_Font_Awesome_Library::get_instance( $args );
	}

	function bc_flex_load_post_types() {			
		
		$options = get_option('flex_settings');
		
		if (get_field('enable_faqs', 'option')) {
			include(plugin_dir_path(__FILE__) . 'defs/faqs.php');
		}
		
		if (get_field('enable_staff', 'option')) {
			include(plugin_dir_path(__FILE__) . 'defs/staff.php');
			include(plugin_dir_path(__FILE__) . 'defs/staff-types.php');
		}
		
		if (get_field('enable_resources', 'option')) {
			include(plugin_dir_path(__FILE__) . 'defs/resources.php');
		}
		
		if (get_field('enable_services', 'option')) {
			include(plugin_dir_path(__FILE__) . 'defs/services.php');
		}
		
		if (get_field('enable_work', 'option')) {
			include(plugin_dir_path(__FILE__) . 'defs/work.php');
		}
		
		if (get_field('enable_case_studies', 'option')) {
			include(plugin_dir_path(__FILE__) . 'defs/case_studies.php');
			include(plugin_dir_path(__FILE__) . 'defs/case_study_meta.php');
		}
		
		if (get_field('enable_member_groups', 'option')) {
			include(plugin_dir_path(__FILE__) . 'defs/student-groups.php');
			include(plugin_dir_path(__FILE__) . 'defs/cohorts.php');
		}
	
	}
	add_action('init', 'bc_flex_load_post_types', 9);	
	

	// PHP Color manipulation lib
	include(plugin_dir_path(__FILE__) . 'inc/phpcolor.php');
	$phpcolor = new Mexitek\PHPColors\Color('#fff');	

	$bc_flex_content = new BC_Flex_Content;
	
?>