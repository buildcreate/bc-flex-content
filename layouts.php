<div class="content-wrap <?php if(get_field('use_sidebar') && !$option) {echo 'has-sidebar';} ?>">
<?php 
	if(have_rows('rows', $option)){
		while(have_rows('rows', $option)){
			the_row();

			// get options for this row
			$row_style = '';
			$row_class = 'content-row';
			$wrapper_style = '';
			$wrapper_class = 'wrapper';			

			$has_overlay = false;
			$overlay_color = false;
			$overlay_opacity = false;
			$overlay_style = '';

			$background_image = false;
			$background_type = get_sub_field('background');
			$background_color = get_sub_field('background_color');
			$background_target = get_sub_field('background_target');

			if($background_type == 'color'){
				$background_style = 'background:'.$background_color. ';';

			}elseif($background_type == 'repeating'){
				$background_image = get_sub_field('background_image');
				$background_style = 'background:url('.$background_image['url']. ');';
				$overlay_color = get_sub_field('overlay_color');
				$overlay_opacity = get_sub_field('overlay_opacity');

			}else{
				$background_image = get_sub_field('background_image');
				$background_style = 'background:url('.$background_image['url']. ') center center;background-size:cover;';
				$overlay_color = get_sub_field('overlay_color');
				$overlay_opacity = get_sub_field('overlay_opacity');
			}

			if($background_target != 'content' && $background_image){
				$row_class .= ' has-image';
			}elseif($background_target == 'content' && $background_image){
				$wrapper_class .= ' has-image';
			}
			
			// check for overlay color
			if($overlay_color && $overlay_opacity){
				$has_overlay = true;
				$overlay_style = 'background: rgba('.hexdec(substr($overlay_color,1,2)).','.hexdec(substr($overlay_color,3,2)).','.hexdec(substr($overlay_color,5,2)).','.($overlay_opacity/100).');';
			}
			
			// maybe get max width
			$max_width = get_sub_field('max_width');
			if($max_width == 0){
				$wrapper_style .= 'width: auto;';
			}elseif($max_width > 100){
				$wrapper_style .= 'width: 100%;';
			}else{
				$wrapper_style .= 'width:'.get_sub_field('max_width').'%;';
			}
			
			// get padding
			$row_style .= 'padding-top:'.get_sub_field('padding_top').'px;';
			$row_style .= 'padding-bottom:'.get_sub_field('padding_bottom').'px;';
			
			// get borders
			$borders = get_sub_field('borders');
			$border_style = '';

			if($borders) {
				$color = get_sub_field('border_color');
				$thickness = get_sub_field('thickness');
		
				if(is_array($borders) && !empty($borders)) {
					foreach($borders as $border) {
						$border_style .= ' border-'.$border.': '.$thickness.'px solid '.$color.';';
					}
				} else {
					$border_style = ' border-'.$borders.': '.$thickness.'px solid '.$color.';';
				}
			}
			get_sub_field('border_target') == 'content' ? $wrapper_style .= $border_style : $row_style .= $border_style;
			
			// get row/wrapper class(es)			
			global $phpcolor;
			if(get_sub_field('hide_on_mobile')){$row_class .= ' mobile-hide';}
			if(get_sub_field('mobile_only')){$row_class .= ' mobile-only';}
			if($custom_class = get_sub_field('class')){$row_class .= ' ' . $custom_class;}
			if($phpcolor->isDark($background_color) || $phpcolor->isDark($overlay_color)){$row_class .= ' dark'; }else{$row_class .= ' light';} 
			
				$block_align = get_sub_field('align_content_blocks');
			?>
			
			<div class="<?php echo $row_class; ?>" style="<?php if($background_target != 'content'){echo $background_style;} ?><?php echo $row_style; ?>">

				<?php if($has_overlay && $background_target != 'content') : ?>
					<div class="content-row-overlay" style="<?php echo $overlay_style; ?>"></div>
				<?php endif; ?>
								
				<div class="<?php echo $wrapper_class; ?>" style="<?php if($background_target == 'content'){echo $background_style;} ?><?php echo $wrapper_style; ?>">

					<?php if($has_overlay && $background_target == 'content') : ?>
						<div class="content-row-overlay" style="<?php echo $overlay_style; ?>"></div>
					<?php endif; ?>

					<?php 
						// reset block widths
						$block_width_total = 0;

						// get layouts in this row
						if(have_rows('row_layout')){
							while(have_rows('row_layout')){
								the_row();
								
								// get block settings
								$block_style = 'padding:';
								$block_style .= get_sub_field('padding_top') ? get_sub_field('padding_top'). 'px ' : 0 . 'px ';
								$block_style .= get_sub_field('padding_right') ? get_sub_field('padding_right'). 'px ' : 0 . 'px ';
								$block_style .= get_sub_field('padding_bottom') ? get_sub_field('padding_bottom'). 'px ' : 0 . 'px ';
								$block_style .= get_sub_field('padding_left') ? get_sub_field('padding_left'). 'px; ' : 0 . 'px; ';
								$block_style .= 'width:'.get_sub_field('block_width').'%; ';
								$block_style .= 'align-self:'.$block_align.';';
								
								$mobile_class = get_sub_field('hide_on_mobile') ? 'mobile-hide' : '';
								$flex_vc = get_sub_field('vertical_center') ? 'flex-vc' : '';
								
								$block_class = $mobile_class.' '.$flex_vc;

									?>
										<div class="content-block <?php echo apply_filters('bc_flex_block_class', $block_class); ?>" style="<?php echo $block_style; ?>">
									<?php
		
								include(plugin_dir_path(__FILE__).'template-parts/'.get_row_layout().'.php');

								//if(!is_home() && !is_category() && !is_archive()) {
									wp_reset_postdata();
								//}
								
								?>
									</div>
								<?php
							}
						}
						?>
					<span class="clearer"></span>
				</div>
			</div>
		<?php		
		}
	}
?>
</div>